﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CrossSolar.Tests
{
    public static class ControllerExtensions
    {
        public static void SetModelStateFromModel(this Microsoft.AspNetCore.Mvc.Controller controller, object model)
        {
            var context = new ValidationContext(model, null, null);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, context, validationResults, true);
            if (isValid)
                return;

            foreach (var validationResult in validationResults)
            {
                foreach (var memberName in validationResult.MemberNames)
                {
                    controller.ModelState.AddModelError(memberName, validationResult.ErrorMessage);
                }
            }
        }
    }
}
