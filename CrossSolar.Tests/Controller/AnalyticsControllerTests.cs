﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrossSolar.Controllers;
using CrossSolar.Domain;
using CrossSolar.Models;
using CrossSolar.Repository;
using Microsoft.AspNetCore.Mvc;
using MockQueryable.Moq;
using Moq;
using Xunit;

namespace CrossSolar.Tests.Controller
{
    public class AnalyticsControllerTests
    {
        private readonly AnalyticsController _analyticsController;
        private const string PanelTestSerial = "AAAA1111BBBB2222";

        public AnalyticsControllerTests()
        {
            var analyticsData = new List<OneHourElectricity>()
            {
                new OneHourElectricity
                {
                    PanelId =  PanelTestSerial,
                    DateTime = DateTime.UtcNow,
                    KiloWatt = 1
                },
                new OneHourElectricity
                {
                    PanelId =  PanelTestSerial,
                    DateTime = DateTime.UtcNow,
                    KiloWatt = 105
                },
                new OneHourElectricity
                {
                    PanelId =  PanelTestSerial,
                    DateTime = DateTime.UtcNow,
                    KiloWatt = 12
                }
            }.AsQueryable().BuildMock();

            var analyticsRepositoryMock = new Mock<IAnalyticsRepository>();
            analyticsRepositoryMock.Setup(x => x.Query()).Returns(analyticsData.Object);

            var panelsData = new List<Panel>()
            {
                new Panel
                {
                    Brand = "Areva",
                    Latitude = 45.312678,
                    Longitude = 76.5543982,
                    Serial = PanelTestSerial
                }
            }.AsQueryable().BuildMock();
            var panelRepositoryMock = new Mock<IPanelRepository>();
            panelRepositoryMock.Setup(x => x.Query()).Returns(panelsData.Object);

            _analyticsController =
                new AnalyticsController(analyticsRepositoryMock.Object, panelRepositoryMock.Object);
        }

        [Fact]
        public async Task Get_ReturnsAnalytics()
        {
            // Arrange
            // Act
            var actionResult = await _analyticsController.Get(PanelTestSerial);

            // Assert
            var okObjectResult = actionResult as OkObjectResult;
            Assert.NotNull(okObjectResult);

            var model = okObjectResult.Value as OneHourElectricityListModel;
            Assert.NotNull(model);
            Assert.NotNull(model.OneHourElectricitys);
            Assert.Equal(3, model.OneHourElectricitys.Count());
        }

        [Fact]
        public async Task DayResults_ReturnsCorrectStats()
        {
            // Arrange
            // Act
            var actionResult = await _analyticsController.DayResults(PanelTestSerial);

            // Assert
            var okObjectResult = actionResult as OkObjectResult;
            Assert.NotNull(okObjectResult);

            var model = (okObjectResult.Value as IEnumerable<OneDayElectricityModel>)?.FirstOrDefault();
            Assert.NotNull(model);
            
            Assert.Equal(DateTime.UtcNow.Date, model.DateTime);
            Assert.Equal(1, model.Minimum);
            Assert.Equal(105, model.Maximum);
            Assert.Equal(39.333333333333336, model.Average);
            Assert.Equal(118, model.Sum);
        }

        [Fact]
        public async Task Post_ShouldInsertAnalytics()
        {
            // Arrange
            var analyticsItem = new OneHourElectricityModel
            {
                KiloWatt = 123
            };

            // Act
            var result = await _analyticsController.Post(PanelTestSerial, analyticsItem);

            // Assert
            Assert.NotNull(result);

            var createdResult = result as CreatedResult;
            Assert.NotNull(createdResult);
            Assert.Equal(201, createdResult.StatusCode);
        }

        [Fact]
        public async Task Post_WontInsertAnalytics()
        {
            // Arrange
            var analyticsItem = new OneHourElectricityModel
            {
                KiloWatt = -123
            };
            _analyticsController.SetModelStateFromModel(analyticsItem);

            // Act
            var result = await _analyticsController.Post(PanelTestSerial, analyticsItem);
            var modelState = _analyticsController.ModelState;

            // Assert
            Assert.NotNull(result);

            var badCreatedResult = result as BadRequestObjectResult;
            Assert.NotNull(badCreatedResult);
            Assert.Equal(400, badCreatedResult.StatusCode);
            Assert.False(modelState.IsValid);
            Assert.False(modelState.Keys.Except(new[]
            {
                nameof(OneHourElectricityModel.KiloWatt),    // KiloWatt is wrong (it's out of range 0 -> 0x7FFFFFFFFFFFFFFF)
            }).Any());
        }
    }
}
