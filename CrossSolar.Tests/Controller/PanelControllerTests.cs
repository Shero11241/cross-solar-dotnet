using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CrossSolar.Controllers;
using CrossSolar.Models;
using CrossSolar.Repository;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace CrossSolar.Tests.Controller
{
    public class PanelControllerTests
    {
        public PanelControllerTests()
        {
            _panelController = new PanelController(_panelRepositoryMock.Object);
        }

        private readonly PanelController _panelController;

        private readonly Mock<IPanelRepository> _panelRepositoryMock = new Mock<IPanelRepository>();

        [Fact]
        public async Task Register_ShouldInsertPanel()
        {
            // Arrange
            var panel = new PanelModel
            {
                Brand = "Areva",
                Latitude = 12.345678,
                Longitude = 98.7655432,
                Serial = "AAAA1111BBBB2222"
            };

            // Act
            var result = await _panelController.Register(panel);

            // Assert
            Assert.NotNull(result);

            var createdResult = result as CreatedResult;
            Assert.NotNull(createdResult);
            Assert.Equal(201, createdResult.StatusCode);
        }

        [Fact]
        public async Task Register_WontInsertAnalytics()
        {
            // Arrange
            var panel = new PanelModel
            {
                Brand = "Areva",
                Latitude = 90,
                Longitude = -198.7655432,
                Serial = "123456789"
            };
            _panelController.SetModelStateFromModel(panel);

            // Act
            var result = await _panelController.Register(panel);
            var modelState = _panelController.ModelState;

            // Assert
            Assert.NotNull(result);

            var badCreatedResult = result as BadRequestObjectResult;
            Assert.NotNull(badCreatedResult);
            Assert.Equal(400, badCreatedResult.StatusCode);
            Assert.False(modelState.IsValid);
            Assert.False(modelState.Keys.Except(new[]
            {
                nameof(PanelModel.Latitude),    // Latitude is wrong (no digits after decimal separator)
                nameof(PanelModel.Longitude),   // Longitude is out of range
                nameof(PanelModel.Serial)       // Serial is too short
            }).Any());
        }
    }
}