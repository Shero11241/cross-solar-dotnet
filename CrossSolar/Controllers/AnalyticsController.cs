﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CrossSolar.Domain;
using CrossSolar.Models;
using CrossSolar.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CrossSolar.Controllers
{
    [Route("panel")]
    public class AnalyticsController : Controller
    {
        private readonly IAnalyticsRepository _analyticsRepository;

        private readonly IPanelRepository _panelRepository;

        public AnalyticsController(IAnalyticsRepository analyticsRepository, IPanelRepository panelRepository)
        {
            _analyticsRepository = analyticsRepository;
            _panelRepository = panelRepository;
        }

        // GET panel/XXXX1111YYYY2222/analytics
        [HttpGet("{panelId}/[controller]")]
        public async Task<IActionResult> Get([FromRoute] string panelId)
        {
            var panel = await _panelRepository.Query()
                .FirstOrDefaultAsync(x => x.Serial.Equals(panelId, StringComparison.CurrentCultureIgnoreCase));

            if (panel == null) return NotFound(); // One shall not see data of non-longer existent panels (there is no rule for cascade delete)

            var result = new OneHourElectricityListModel
            {
                OneHourElectricitys = _analyticsRepository
                    .Query()
                    .Where(a => a.PanelId.Equals(panelId, StringComparison.CurrentCultureIgnoreCase))
                    .Select(c => new OneHourElectricityModel
                    {
                        Id = c.Id,
                        KiloWatt = c.KiloWatt,
                        DateTime = c.DateTime
                    })
            };

            return Ok(result);
        }

        // GET panel/XXXX1111YYYY2222/analytics/day
        [HttpGet("{panelId}/[controller]/day")]
        public async Task<IActionResult> DayResults([FromRoute] string panelId)
        {
            var panel = await _panelRepository.Query()
                .FirstOrDefaultAsync(x => x.Serial.Equals(panelId, StringComparison.CurrentCultureIgnoreCase));

            if (panel == null) return NotFound(); // One shall not see data of non-longer existent panels (there is no rule for cascade delete)

            var result = _analyticsRepository
                .Query()
                .Where(a => a.PanelId.Equals(panelId, StringComparison.CurrentCultureIgnoreCase))
                .GroupBy(a => a.DateTime.Date)
                .AsParallel()
                .Select(g => new OneDayElectricityModel
                {
                    DateTime = g.Key,
                    Sum = g.Sum(a => a.KiloWatt),
                    Minimum = g.Min(a => a.KiloWatt),
                    Maximum = g.Max(a => a.KiloWatt),
                    Average = g.Average(a => a.KiloWatt)
                });

            return Ok(result);
        }

        // POST panel/XXXX1111YYYY2222/analytics
        [HttpPost("{panelId}/[controller]")]
        public async Task<IActionResult> Post([FromRoute] string panelId, [FromBody] OneHourElectricityModel value)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var panel = await _panelRepository
                .Query()
                .FirstOrDefaultAsync(x => x.Serial.Equals(panelId, StringComparison.CurrentCultureIgnoreCase));

            if (panel == null) return NotFound();

            var result = new OneHourElectricity
            {
                PanelId = panelId,
                KiloWatt = value.KiloWatt,
                DateTime = DateTime.UtcNow
            };

            await _analyticsRepository.InsertAsync(result);

            return Created($"panel/{panelId}/analytics/{result.Id}", result);
        }
    }
}