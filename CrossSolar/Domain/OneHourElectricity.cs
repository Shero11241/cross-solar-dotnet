﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CrossSolar.Domain
{
    public class OneHourElectricity
    {
        [Key]
        public int Id { get; set; }

        [Required] public string PanelId { get; set; }

        [Required] public long KiloWatt { get; set; }

        [Required] public DateTime DateTime { get; set; }
    }
}