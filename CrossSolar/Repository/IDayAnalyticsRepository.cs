﻿using CrossSolar.Models;

namespace CrossSolar.Repository
{
    public interface IDayAnalyticsRepository : IGenericRepository<OneDayElectricityModel>
    {
    }
}